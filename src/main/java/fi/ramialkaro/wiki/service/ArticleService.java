package fi.ramialkaro.wiki.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.ramialkaro.wiki.service.DTO.WikiResponseDTO;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ArticleService {

    final String URL = "https://en.wikipedia.org/w/api.php?action=query&list=search&format=json&srsearch=";
    // final String testURL = "https://jsonplaceholder.typicode.com/todos/";

    final RestTemplate restTemplate;

    public ArticleService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public WikiResponseDTO getArticles(String keyword) throws JsonProcessingException {
        WikiResponseDTO request = new WikiResponseDTO();
        HttpEntity<String> entity = getWikiRequestEntity(request);

        String res = restTemplate.postForObject(URL + keyword, entity, String.class);
        WikiResponseDTO resDTO = new ObjectMapper().readValue(res, WikiResponseDTO.class);
        if (resDTO == null) {
            throw new IllegalStateException("error with data");
        }
        return resDTO;
    }

    private HttpEntity<String> getWikiRequestEntity(WikiResponseDTO request) throws JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(request.toJson(), headers);
    }
}
