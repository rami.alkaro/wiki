package fi.ramialkaro.wiki.service.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class WikiResponseDTO  implements RequestDTO{
    @JsonProperty("batchcomplete")
    private String batchComplete;
    @JsonProperty("continue")
    private Continue continueObj;
    private Query query;

    public WikiResponseDTO() {
    }

    public String getBatchComplete() {
        return batchComplete;
    }

    public void setBatchComplete(String batchComplete) {
        this.batchComplete = batchComplete;
    }

    public Continue getContinueObj() {
        return continueObj;
    }

    public void setContinueObj(Continue continueObj) {
        this.continueObj = continueObj;
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    @Override
    public String toString() {
        return "WikiResponseDTO{" +
                "batchComplete='" + batchComplete + '\'' +
                ", continueObj=" + continueObj +
                ", query=" + query +
                '}';
    }

    @Override
    public String toJson() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(this);
    }
}
