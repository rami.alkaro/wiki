package fi.ramialkaro.wiki.service.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Continue {
    private Integer sroffset;
    @JsonProperty("continue")
    private String continueObj;

    public Continue() {
    }

    public Integer getSroffset() {
        return sroffset;
    }

    public void setSroffset(Integer sroffset) {
        this.sroffset = sroffset;
    }

    public String getContinueObj() {
        return continueObj;
    }

    public void setContinueObj(String continueObj) {
        this.continueObj = continueObj;
    }

    @Override
    public String toString() {
        return "Continue{" +
                "sroffset=" + sroffset +
                ", continueObj='" + continueObj + '\'' +
                '}';
    }
}
