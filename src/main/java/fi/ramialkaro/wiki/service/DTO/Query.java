package fi.ramialkaro.wiki.service.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Query {
    @JsonProperty("searchinfo")
    private SearchInfo searchInfo;
    @JsonProperty("search")
    private List<Match> search = new ArrayList<>();

    public Query() {
    }

    public SearchInfo getSearchInfo() {
        return searchInfo;
    }

    public void setSearchInfo(SearchInfo searchInfo) {
        this.searchInfo = searchInfo;
    }

    public List<Match> getSearch() {
        return search;
    }

    public void setSearch(List<Match> search) {
        this.search = search;
    }

    @Override
    public String toString() {
        return "Query{" +
                "searchInfo=" + searchInfo +
                ", search=" + search +
                '}';
    }
}
