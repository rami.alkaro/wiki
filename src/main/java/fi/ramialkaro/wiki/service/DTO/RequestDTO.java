package fi.ramialkaro.wiki.service.DTO;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface RequestDTO {
    public String toJson() throws JsonProcessingException;
}
