package fi.ramialkaro.wiki.service.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.SneakyThrows;
import org.thymeleaf.util.DateUtils;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Match {

    private Integer ns;
    private String title;
    @JsonProperty("pageid")
    private Integer pageId;
    private Integer size;
    @JsonProperty("wordcount")
    private Integer wordCount;
    private String snippet;
    private String timestamp;

    public Match() {
    }

    ;

    public Integer getNs() {
        return ns;
    }

    public void setNs(Integer ns) {
        this.ns = ns;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getPageId() {
        return pageId;
    }

    public void setPageId(Integer pageId) {
        this.pageId = pageId;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getWordCount() {
        return wordCount;
    }

    public void setWordCount(Integer wordCount) {
        this.wordCount = wordCount;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String convertTimestampToZoneDateTime() {
        return ZonedDateTime.parse(this.timestamp).format(DateTimeFormatter.ofPattern("EEE, MMM dd, yyyy hh:mm"));
    }

    @SneakyThrows
    @Override
    public String toString() {
        return "Match{" +
                "ns=" + ns +
                ", title='" + title + '\'' +
                ", pageId=" + pageId +
                ", size=" + size +
                ", wordCount=" + wordCount +
                ", snippet='" + snippet + '\'' +
                ", timestamp=" + convertTimestampToZoneDateTime() +
                '}';
    }
}
