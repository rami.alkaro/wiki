package fi.ramialkaro.wiki.service.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SearchInfo {
    @JsonProperty("totalhits")
    private Integer totalhits;
    private String suggestion;
    @JsonProperty("suggestionsnippet")
    private String suggestionSnippet;

    public SearchInfo() {
    }

    public Integer getTotalhits() {
        return totalhits;
    }

    public void setTotalhits(Integer totalhits) {
        this.totalhits = totalhits;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    public String getSuggestionSnippet() {
        return suggestionSnippet;
    }

    public void setSuggestionSnippet(String suggestionSnippet) {
        this.suggestionSnippet = suggestionSnippet;
    }

    @Override
    public String toString() {
        return "SearchInfo{" +
                "totalhits=" + totalhits +
                ", suggestion='" + suggestion + '\'' +
                ", suggestionSnippet='" + suggestionSnippet + '\'' +
                '}';
    }
}
