package fi.ramialkaro.wiki.web.view;

import com.fasterxml.jackson.core.JsonProcessingException;
import fi.ramialkaro.wiki.service.ArticleService;
import fi.ramialkaro.wiki.service.DTO.WikiResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class homeController {

    private final ArticleService articleService;
    private final Logger log = LoggerFactory.getLogger(homeController.class);

    public homeController(ArticleService articleService) {
        this.articleService = articleService;
    }


    @GetMapping("home")
    public String homePage(@RequestParam(required = false) String search,  Model model) throws JsonProcessingException {
        WikiResponseDTO articles = articleService.getArticles(search);
        System.out.println(articles);
        model.addAttribute("articles", articles);
        model.addAttribute("size", articles.getQuery().getSearch().size());
        return "home";
    }

}
